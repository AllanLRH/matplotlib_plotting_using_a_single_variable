2D-plotting in Matplotlib with $x$ and $y$ from a single variable.

[Nbviewer-link](http://nbviewer.jupyter.org/urls/bitbucket.org/AllanLRH/matplotlib_plotting_using_a_single_variable/raw/be2179b25dd4b711359afb1d6fd0a8d59c4124c3/matplotlib_plotting_using_a_single_variable.ipynb)
